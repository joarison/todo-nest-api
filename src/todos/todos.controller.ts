import {Controller, Get, Post, Patch, Put, Delete, Body, Param, Query} from '@nestjs/common';
import {TodosService} from "./todos.service";
import {Todo} from "./interfaces/todo.interface";
import {CreateTodoDto} from "./Dto/create-todo.dto";


// localhost:3000/todos
@Controller('todos')
export class TodosController {
    constructor(
        private readonly todosService: TodosService
    ) {
    }

    @Get()
    findAllTodos(): Todo[]{
        return this.todosService.findAll();
    }

    @Get(':id')
    findOneTodo(@Param('id') id: string) {
        console.log('id todo ', id)
        return this.todosService.findOne(id);
    }

    @Post('body')
    createTodoWithBody(@Body() newTodo: CreateTodoDto) {
        console.log('nouveau body todo ', newTodo)
        this.todosService.createBody(newTodo);
    }

    @Post('query')
    createTodoWithQuery(@Query() newTodo) {
        console.log('nouveau query todo ', newTodo)
        this.todosService.createQuery(newTodo);
    }

    @Patch(':id')
    updateTodo(@Param('id') id: string, @Body() todo: CreateTodoDto){
        return this.todosService.updatePatch(id, todo);
    }

    @Delete(':id')
    deleteTodo(@Param('id') id: string) {
        return this.todosService.delete(id);
    }
}

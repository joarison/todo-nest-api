import {Injectable, NotFoundException} from '@nestjs/common';
import {Todo} from "./interfaces/todo.interface";
import {CreateTodoDto} from "./Dto/create-todo.dto";

@Injectable()
export class TodosService {
    todos: Todo[] = [
        {
            id: 1,
            title: 'todo app',
            description: 'create NestJS todos app',
            done: false,
        },
        {
            id: 2,
            title: 'bread',
            description: 'buy bread',
            done: true,
        },
 {
            id: 3,
            title: 'wine',
            description: 'buy wine',
            done: true,
        }

    ];

    findAll(): Todo[]{
        return this.todos;
    }

    findOne(id: string){
        return this.todos.find(todo => todo.id === Number(id));
    }

    createBody(todo: CreateTodoDto) {
        this.todos = [...this.todos, todo as Todo];
    }

    createQuery(todo: Todo) {
        this.todos = [...this.todos, todo];
    }

    updatePatch(id: string, todo: Todo){
        // retrieve the todo to update patch
        const todoToUpdate = this.todos.find(t => t.id === +id);
        if (!todoToUpdate){
            return new NotFoundException('noooo did you find this todo');
        }
        // apply to granulary update a single property
        if (todo.hasOwnProperty('done')){
            todoToUpdate.done = todo.done;
        }
        if (todo.title){
            todoToUpdate.title = todo.title;
        }
        if (todo.description){
            todoToUpdate.description = todo.description;
        }
        const updatedTodos = this.todos.map(t => t.id !== +id ? t: todoToUpdate);
        this.todos = [...updatedTodos];

        return { updatedTodo: 1, todo: todoToUpdate};
    }

    delete(id: string) {
        const nbOfTodoBeforeDelete = this.todos.length;
        this.todos = [...this.todos.filter(t => t.id !== +id)];
        if (this.todos.length < nbOfTodoBeforeDelete){
            return { deletedTodo: 1, nbTodos: this.todos.length};
        } else {
            return { deletedTodo: 0, nbTodos: this.todos.length};
        }
    }
}

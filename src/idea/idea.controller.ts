import {Body, Controller, Delete, Get, Param, Post, Put, Query} from '@nestjs/common';
import {IdeaService} from "./idea.service";
import {IdeaDto} from "./idea.dto";

@Controller('idea')
export class IdeaController {

    constructor(
        private ideaService: IdeaService
    ) {
    }

    @Get()
    showAllIdeas(){
        return this.ideaService.getAll();
    }

    @Post()
    creatIdeas(@Body() data: IdeaDto){
        console.log('data idea create: ', data)
        return this.ideaService.create(data);
    }

    @Get(':id')
    readIdea(@Param('id') id: string){
        return this.ideaService.getOne(id);
    }

    @Put(':id')
    updateIdea(@Param('id') id: string, @Body() data: Partial<IdeaDto>){
        return this.ideaService.update(id, data);
    }

    @Delete(':id')
    destroyIdea(@Param('id') id: string){
        return this.ideaService.delete(id);
    }
}
